#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <L2cache.h>
#include <Victimcache.h>
#include <debug_utilities.h>
#include <time.h>

using namespace std;


/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  clock_t start_time = clock();
  cache_params params;
  int RP;
  for (size_t i = 1; i < argc; i++) {
      if (strcmp(argv[i],"-s")==0){
        params.size = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-l")==0){
        params.block_size = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-a")==0){
        params.asociativity = atoi(argv[i+1]);
      }else if(strcmp(argv[i],"-rp")==0){
        if(strcmp(argv[i+1], "0")==0){
          RP = LRU;
        }
        if(strcmp(argv[i+1],"1")==0){
          RP = NRU;
        }
        if(strcmp(argv[i+1],"2")==0){
          RP = RRIP;
        }
      }
  }

  simulate_L1(RP, params);




  



  printf("Tiempo transcurrido: %f\n", ((double)clock() - start_time) / CLOCKS_PER_SEC);

  /* Parse argruments */

  /* Get trace's lines and start your simulation */

  /* Print cache configuration */

  /* Print Statistics */
return 0;
}
