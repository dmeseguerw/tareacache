/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				operation_result* operation_result_cache_obl,
                                bool debug=false)
{
        if(idx < 0 || tag < 0 || associativity < 1){return PARAM;}

        if(loadstore) {
                operation_result_cache_block->miss_hit = MISS_STORE;
                operation_result_cache_obl->miss_hit = MISS_STORE;
        }
        else{
                operation_result_cache_block->miss_hit = MISS_LOAD;
                operation_result_cache_obl->miss_hit = MISS_LOAD;
        }

        bool prefetch = false;

        // Verificar si hay un hit
        for(size_t i = 0; i <associativity; i++){//Si valid es 0, no hay nada en el bloque de cache todavia
                if(cache_block[i].valid){
                        if(cache_block[i].tag == tag){//Esto es un hit
                                if(loadstore) {
                                        cache_block[i].dirty = true;
                                        operation_result_cache_block->miss_hit = HIT_STORE;
                                }
                                else{operation_result_cache_block->miss_hit = HIT_LOAD;}

                                // Update LRUs
                                for(size_t j = 0; j < associativity; j++){
                                        if(cache_block[j].rp_value > cache_block[i].rp_value) cache_block[j].rp_value = cache_block[j].rp_value-1;
                                }
                                cache_block[i].rp_value = associativity-1;

                                if (cache_block[i].obl_tag){
                                        cache_block[i].obl_tag = false;
                                        prefetch = true;
                                }

                            
                        }
                }
        }


        // Ver si hay un miss
        if(operation_result_cache_block->miss_hit == MISS_LOAD || operation_result_cache_block->miss_hit == MISS_STORE){

                //Como hay un miss, busco el LRU para reemplazarlo
                size_t lru_way = 0;
                for(size_t j = 0; j < associativity; j++){ //Busco el LRU
                        if(cache_block[j].rp_value < cache_block[lru_way].rp_value) lru_way = j;
                }

                //Guardo el evicted
                if(cache_block[lru_way].valid){
                        operation_result_cache_block->dirty_eviction = cache_block[lru_way].dirty;
                        operation_result_cache_block->evicted_address = cache_block[lru_way].tag;
                }

                //Reemplazo valores
                cache_block[lru_way].tag = tag;
                cache_block[lru_way].valid = 1;
                cache_block[lru_way].obl_tag = false;
                prefetch = true;

                if(loadstore){
                        cache_block[lru_way].dirty = true;
                        operation_result_cache_block->miss_hit = MISS_STORE;
                }
                else{operation_result_cache_block->miss_hit = MISS_LOAD;}

                //Reacomodo el LRU
                for(size_t j = 0; j < associativity; j++){
                        if(cache_block[j].rp_value > cache_block[lru_way].rp_value) cache_block[j].rp_value = cache_block[j].rp_value-1;
                }
                cache_block[lru_way].rp_value = associativity-1;
        }

        if (prefetch){
                // Verificar si hay un hit
                for(size_t i = 0; i <associativity; i++){//Si valid es 0, no hay nada en el bloque de cache todavia
                        cache_block_obl[i].dirty = false;
                        if(cache_block_obl[i].valid){
                                if(cache_block_obl[i].tag == tag){//Esto es un hit
                                        cache_block_obl[i].obl_tag == true;
                                        if(loadstore) {operation_result_cache_obl->miss_hit = HIT_STORE;}
                                        else{operation_result_cache_obl->miss_hit = HIT_LOAD;}

                                        // Update LRUs
                                        for(size_t j = 0; j < associativity; j++){
                                                if(cache_block_obl[j].rp_value > cache_block_obl[i].rp_value) cache_block_obl[j].rp_value = cache_block_obl[j].rp_value-1;
                                        }
                                        cache_block_obl[i].rp_value = associativity-1;

                                        return OK;
                                }
                        }
                }


                // Ver si hay un miss
                if(operation_result_cache_obl->miss_hit == MISS_LOAD || operation_result_cache_obl->miss_hit == MISS_STORE){
                        //Como hay un miss, busco el LRU para reemplazarlo
                        size_t lru_way = 0;
                        for(size_t j = 0; j < associativity; j++){ //Busco el LRU
                                if(cache_block_obl[j].rp_value < cache_block_obl[lru_way].rp_value) lru_way = j;
                        }

                        //Guardo el evicted
                        if(cache_block_obl[lru_way].valid){
                                operation_result_cache_obl->dirty_eviction = cache_block_obl[lru_way].dirty;
                                operation_result_cache_obl->evicted_address = cache_block_obl[lru_way].tag;
                        }

                        //Reemplazo valores
                        cache_block_obl[lru_way].tag = tag;
                        cache_block_obl[lru_way].valid = 1;
                        cache_block_obl[lru_way].obl_tag = true;
                        if(loadstore){operation_result_cache_obl->miss_hit = MISS_STORE;}
                        else{operation_result_cache_obl->miss_hit = MISS_LOAD;}

                        //Reacomodo el LRU
                        for(size_t j = 0; j < associativity; j++){
                                if(cache_block_obl[j].rp_value > cache_block_obl[lru_way].rp_value) cache_block_obl[j].rp_value = cache_block_obl[j].rp_value-1;
                        }
                        cache_block_obl[lru_way].rp_value = associativity-1;
                        
                        return OK;
                }
        }
        else{return OK;}
        return ERROR;
}
