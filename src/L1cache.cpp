/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <tgmath.h> 

using namespace std;


#define KB 1024
#define ADDRSIZE 32
using namespace std;



cache_field_size field_size_get(struct cache_params cache_params)
{
   cache_field_size field_size;
   field_size.offset = int(log2((double)cache_params.block_size));
   field_size.idx = log2(cache_params.size*(KB)/(cache_params.asociativity*cache_params.block_size));
   field_size.tag = ADDRSIZE - field_size.offset - field_size.idx;
 
  return field_size;
}

cache_field_size address_tag_idx_get(long address,
                         struct cache_field_size field_size
                         )
{

   field_size.actual_idx = (address >> field_size.offset) & (int)(pow(2, field_size.idx)-1);
   field_size.actual_tag = address >> (field_size.offset + field_size.idx); 

   return field_size;

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }

   if(loadstore) {//Comenzamos en miss
               result->miss_hit = MISS_STORE;
               }
   else{
      result->miss_hit = MISS_LOAD;
   }

   //Verificar si hay un hit
   for(size_t i = 0; i <associativity; i++){//Si valid es 0, no hay nada en el bloque de cache todavia

      if(cache_blocks[i].valid){
         if(cache_blocks[i].tag == tag){//Esto es un hit
            cache_blocks[i].rp_value = 0; //Update el rrip rrpv bit a 0

            if(loadstore) {
               cache_blocks[i].dirty = true;
               result->miss_hit = HIT_STORE;
               }
            else{result->miss_hit = HIT_LOAD;}

         
         return OK;
         }
      }
   }

   int rrip_index = 0;
   bool found1 = false;
   int M;
   // se toma M=1 si la asociatividad es menor o igual a dos y M=2 si la asociatividad es mayor a dos. 
   if (associativity <= 2){ M = 1;}
   if (associativity > 2){ M = 2;}
   // con el valor lejano se determina el valor que se coloca en rrpv cuando ingresa un bloque
   int lejano = pow(2, M) - 2;
   // con el valor distante se determina el valor que hay que buscar para seleccionar el bloque a reemplazar
   int distante = pow(2, M) - 1;

   //Ver si hay un miss
   if(result->miss_hit == MISS_LOAD || result->miss_hit == MISS_STORE){
      
      // Como hay un MISS, busco el primer rp value rrpv = 3 de izquierda a derecha.
      while(found1 == false){
         for(size_t j = 0; j < associativity; j++){
            if(cache_blocks[j].rp_value == distante){
               rrip_index = j;
               found1 = true;
               break;
            }    
         }
         //Si no encuentro un 3, sumo 1 a todos y vuelvo a buscar el primer 3
         if(found1 == false){
            for(size_t j = 0; j < associativity; j++){
               cache_blocks[j].rp_value = distante;
            }  
         }
      }

      //Guardo el evicted. SOLO GUARDO SI YA ESTA LLENO, TODOS LOS VALIDS SON 1
      if(cache_blocks[rrip_index].valid){
         result->dirty_eviction = cache_blocks[rrip_index].dirty;
         result->evicted_address = cache_blocks[rrip_index].tag;
      }

      //Reemplazo valores
      cache_blocks[rrip_index].tag = tag;
      // asignar el rrpv como el valor lejano. 
      cache_blocks[rrip_index].rp_value = lejano; 
      cache_blocks[rrip_index].valid = 1;

      if(loadstore){
         cache_blocks[rrip_index].dirty = true;
         result->miss_hit = MISS_STORE;
      }
      else{result->miss_hit = MISS_LOAD;}

      return OK;

   }
   return ERROR;
}




int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }


   if(loadstore) {//Comenzamos en miss
               result->miss_hit = MISS_STORE;
               }
   else{
      result->miss_hit = MISS_LOAD;
   }


   // Verificar si hay un hit
   for(size_t i = 0; i <associativity; i++){//Si valid es 0, no hay nada en el bloque de cache todavia
      if(cache_blocks[i].valid){
         if(cache_blocks[i].tag == tag){//Esto es un hit
            if(loadstore) {
               cache_blocks[i].dirty = true;
               result->miss_hit = HIT_STORE;
               }
            else{result->miss_hit = HIT_LOAD;}

            // Update LRUs
            for(size_t j = 0; j < associativity; j++){
               if(cache_blocks[j].rp_value > cache_blocks[i].rp_value) cache_blocks[j].rp_value = cache_blocks[j].rp_value-1;
            }
            cache_blocks[i].rp_value = associativity-1;
            return OK;
         }
      }

   }


   // Ver si hay un miss
   if(result->miss_hit == MISS_LOAD || result->miss_hit == MISS_STORE){

      //Como hay un miss, busco el LRU para reemplazarlo
      size_t lru_way = 0;
      for(size_t j = 0; j < associativity; j++){ //Busco el LRU
         if(cache_blocks[j].rp_value < cache_blocks[lru_way].rp_value) lru_way = j;
      }



      //Guardo el evicted
      if(cache_blocks[lru_way].valid){
         result->dirty_eviction = cache_blocks[lru_way].dirty;
         result->evicted_address = cache_blocks[lru_way].tag;
      }


      //Reemplazo valores
      cache_blocks[lru_way].tag = tag;
      cache_blocks[lru_way].valid = 1;

      if(loadstore){
         cache_blocks[lru_way].dirty = true;
         result->miss_hit = MISS_STORE;
      }
      else{result->miss_hit = MISS_LOAD;}

      //Reacomodo el LRU
      for(size_t j = 0; j < associativity; j++){
         if(cache_blocks[j].rp_value > cache_blocks[lru_way].rp_value) cache_blocks[j].rp_value = cache_blocks[j].rp_value-1;
      }
      cache_blocks[lru_way].rp_value = associativity-1;
      
      return OK;

   }
   return ERROR;
   }


int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1){
      return PARAM;
   }

   if(loadstore) {//Comenzamos en miss
               operation_result->miss_hit = MISS_STORE;
               }
   else{
      operation_result->miss_hit = MISS_LOAD;
   }


   //Verificar si hay un hit
   for(size_t i = 0; i <associativity; i++){//Si valid es 0, no hay nada en el bloque de cache todavia
      if(cache_blocks[i].valid){
         if(cache_blocks[i].tag == tag){//Esto es un hit
            cache_blocks[i].rp_value = 0; //Update el nru R bit

            if(loadstore) {
               cache_blocks[i].dirty = true;
               operation_result->miss_hit = HIT_STORE;
               }
            else{operation_result->miss_hit = HIT_LOAD;}

         
         return OK;
         }
      }
   }

   int nru_index = 0;
   bool found1 = false;


   //Ver si hay un miss
   if(operation_result->miss_hit == MISS_LOAD || operation_result->miss_hit == MISS_STORE){
      
      // Como hay un MISS, busco el primer rp value = 1.
      for(size_t j = 0; j < associativity; j++){
         if(cache_blocks[j].rp_value){
            nru_index = j;
            found1 = true;
            break;
         }    
      }
      
      //Si no encuentro un 1, hago todos igual a 1 y el que se va a reemplazar es el primero
      if(found1 == false){
         for(size_t j = 0; j < associativity; j++){
            cache_blocks[j].rp_value = 1;
         }
         nru_index = 0;
      }





      //Guardo el evicted. SOLO GUARDO SI YA ESTA LLENO, TODOS LOS VALIDS SON 1
      if(cache_blocks[nru_index].valid){
         operation_result->dirty_eviction = cache_blocks[nru_index].dirty;
         operation_result->evicted_address = cache_blocks[nru_index].tag;
      }

      //Reemplazo valores
      cache_blocks[nru_index].tag = tag;
      cache_blocks[nru_index].rp_value = 0;
      cache_blocks[nru_index].valid = 1;

      if(loadstore){
         cache_blocks[nru_index].dirty = true;
         operation_result->miss_hit = MISS_STORE;
      }
      else{operation_result->miss_hit = MISS_LOAD;}

      return OK;

   }
   
   return ERROR;
}




int simulate_L1(int RP, cache_params params){
   //Simulacion LRU
   cache_field_size field_size = field_size_get(params); //Size in bits
   struct entry cache_blocks[(int)pow(2,field_size.idx)][params.asociativity];
   struct operation_result result[(int)pow(2,field_size.idx)];
   string ln;
   int miss_load = 0;
   int miss_store = 0;
   int hit_load = 0;
   int hit_store = 0;
   int dirties = 0;
   bool loadstore;

   int M;
   // se toma M=1 si la asociatividad es menor o igual a dos y M=2 si la asociatividad es mayor a dos. 
   if (params.asociativity <= 2){ M = 1;}
   if (params.asociativity > 2){ M = 2;}
   // Valores para inicializar los bloques en rrip
   int distante = pow(2, M) - 1;

   //Inicializo los valores de LRU
   for(int i = 0; i<(int)pow(2,field_size.idx); i++){
      for(int j =0; j < params.asociativity; j++){
         if(RP==0) cache_blocks[i][j].rp_value = j;
         if(RP==1) cache_blocks[i][j].rp_value = 0;
         if(RP==2) cache_blocks[i][j].rp_value = distante;
         cache_blocks[i][j].tag = 0;
         cache_blocks[i][j].dirty = 0;
         cache_blocks[i][j].valid = 1;

      }
   }

   int counter = 0;
   int IC = 0;
   string ic;
   while(getline(cin, ln)){
      counter++;
      //Manipulo la entrada de instrucciones
      ic = ln.back();
      IC += stoi(ic); // el utilimo bloque son las instrucciones por ciclo que se ocupan para el CPU time
      string address = ln.substr(4, 8);
      long a = strtol(address.c_str(), NULL, 16);
      field_size = address_tag_idx_get(a, field_size);
      if(ln.at(2) == '1') loadstore = 1;
      if(ln.at(2) == '0') loadstore = 0;
      // cout << "INDEX: " << field_size.actual_idx << "\n";
      // cout << "ANTES \n";
      for (int i =0; i < params.asociativity; i++){
         // cout << "R value: " << (int)cache_blocks[field_size.actual_idx][i].rp_value << " | " << "Dirty: " << cache_blocks[field_size.actual_idx][i].dirty << "\n";
         // cout << "Searched tag: " << field_size.actual_tag << " | " << "Tag in cache: " << cache_blocks[field_size.actual_idx][i].tag << "\n";
      }
      // IC += ln.at(2);
   //Ejecuto la politica de reemplazo sobre el set correspondiente

   if(RP==0) lru_replacement_policy(field_size.actual_idx, field_size.actual_tag, params.asociativity, loadstore,
                           cache_blocks[field_size.actual_idx], &result[field_size.actual_idx]);

   if(RP==1) nru_replacement_policy(field_size.actual_idx, field_size.actual_tag, params.asociativity, loadstore,
                           cache_blocks[field_size.actual_idx], &result[field_size.actual_idx]);

   if(RP==2) srrip_replacement_policy(field_size.actual_idx, field_size.actual_tag, params.asociativity, loadstore,
                        cache_blocks[field_size.actual_idx], &result[field_size.actual_idx]);
      // cout << "DESPUES \n";
      for (int i =0; i < params.asociativity; i++){
         // cout << "R value: " << (int)cache_blocks[field_size.actual_idx][i].rp_value << " | " << "Dirty: " << cache_blocks[field_size.actual_idx][i].dirty << "\n";
         // cout << "Searched tag: " << field_size.actual_tag << " | " << "Tag in cache: " << cache_blocks[field_size.actual_idx][i].tag << "\n";
      }

      // cout << "RESULTADO: " << result[field_size.actual_idx].miss_hit << "\n";

   

   // Obtengo los resultados
   if(result[field_size.actual_idx].miss_hit == MISS_LOAD) miss_load++;
   if(result[field_size.actual_idx].miss_hit == MISS_STORE) miss_store++;
   if(result[field_size.actual_idx].miss_hit == HIT_LOAD) hit_load++;
   if(result[field_size.actual_idx].miss_hit == HIT_STORE) hit_store++;
   if(result[field_size.actual_idx].dirty_eviction == 1) dirties++; 
    
   
    
  }
  IC = IC/counter;



  
simulation_results(params, miss_load, miss_store, hit_load, hit_store, dirties, IC);

return OK;
}



int simulation_results(cache_params params, int miss_load, int miss_store, int hit_load, int hit_store, int dirties, int IC){
   long missrate = (miss_load + miss_store)*100/(miss_load+hit_load + miss_store + hit_store);
   long instrucciones = hit_load + hit_store + miss_load + miss_store;
   long CPU_time = (IC/instrucciones)*(1 + (missrate*20));
   cout << "_______________________________________________________" << " \n " << "Cache Parameters \n" << "_______________________________________________________ \n";
   cout << " Cache Size (KB):    " << params.size << "\n";
   cout << " Cache Associativity:    " << params.asociativity << "\n";
   cout << " Cache Block Size (bytes):    " << params.block_size << "\n";
   cout << "_______________________________________________________" << " \n " << "Simulation results \n" << "_______________________________________________________ \n";
   // CPU time: # instr * ciclos de intruccion * ciclos de reloj
   cout << " CPU time (cycles):  " << CPU_time << "\n";
   //Avarage memory acces time: hit_time + missrate * misspenalty
   cout << " AMAT (cycles):   " << 1 + (((miss_load + miss_store)*100/(miss_load+hit_load + miss_store + hit_store))*20) << "\n";
   cout << " Overall miss rate:    " << (miss_load + miss_store)*100/(miss_load+hit_load + miss_store + hit_store) << "% \n";
   cout << " Read miss rate:    " << miss_load*100/(miss_load+hit_load) << "% \n";
   cout << " Dirty evictions:    " << dirties << "\n";
   cout << " Load misses:    " << miss_load <<"\n";
   cout << " Store misses:    " << miss_store << "\n";
   cout << " Total misses:    " << miss_load+miss_store << "\n";
   cout << " Load hits:    " << hit_load << "\n";
   cout << " Store hits:    " << hit_store << "\n";
   cout << " Total hits:    " << hit_load + hit_store << "\n";
   cout << "_______________________________________________________" << " \n ";

   return OK;
}

