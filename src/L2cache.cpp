/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;


int l1_l2_entry_info_get (const struct cache_params l1_params,
                          const struct cache_params l2_params,
						  long address,
						  entry_info* l1_l2_info,
                          bool debug){
	
	l1_l2_info->l1_assoc = l1_params.asociativity;
	l1_l2_info->l1_idx = (address >> (int)log2((double)l1_params.block_size)) & ((l1_params.size*(KB)/(l1_params.asociativity*l1_params.block_size))-1);
	l1_l2_info->l1_tag = address >> ((int)log2((double)l1_params.block_size) + (int)log2(l1_params.size*(KB)/(l1_params.asociativity*l1_params.block_size)));


	l1_l2_info->l2_assoc = l2_params.asociativity;
	l1_l2_info->l2_idx = (address >> (int)log2((double)l2_params.block_size)) & ((l2_params.size*(KB)/(l2_params.asociativity*l2_params.block_size))-1);
	l1_l2_info->l2_tag = address >> ((int)log2((double)l2_params.block_size) + (int)log2(l2_params.size*(KB)/(l2_params.asociativity*l2_params.block_size)));

	return OK;
			  }

int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{
	if(l1_l2_info->l1_idx < 0 || l1_l2_info->l1_tag < 0 || l1_l2_info->l1_assoc < 1 || l1_l2_info->l2_idx < 0 || l1_l2_info->l2_tag < 0 || l1_l2_info->l2_assoc < 1){
      return PARAM;
   	}

	if(loadstore) {//Comenzamos en miss
		l1_result->miss_hit = MISS_STORE;
		}
	else{
		l1_result->miss_hit = MISS_LOAD;
	}

	//CASO HIT L1
	for(size_t i = 0; i < l1_l2_info->l1_assoc; i++){
		if(l1_cache_blocks[i].valid){
			if(l1_cache_blocks[i].tag == l1_l2_info->l1_tag){//Hit en L1

				//Update LRU
				for(size_t j = 0; j < l1_l2_info->l1_assoc; j++){
					if(l1_cache_blocks[j].rp_value > l1_cache_blocks[i].rp_value) l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value-1;
				}
				l1_cache_blocks[i].rp_value = l1_l2_info->l1_assoc-1;
				
				//Update al resultado
				if(loadstore){
					l1_result->miss_hit = HIT_STORE;
					//Como es write through, tengo que ir a darselo a L2 de una vez, para que marque este bloque como dirty.
					//
					for(size_t k = 0; k < l1_l2_info->l2_assoc; k++){
						if(l2_cache_blocks[k].tag == l1_l2_info->l2_tag){
							l2_cache_blocks[k].dirty = 1;
						}
					}
					l2_result->miss_hit = HIT_STORE;
					//
					//
					for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){
						if(l2_cache_blocks[j].rp_value > l2_cache_blocks[i].rp_value) l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
					}
					l2_cache_blocks[i].rp_value = l1_l2_info->l2_assoc-1;
				}
				else{
					l1_result->miss_hit = HIT_LOAD;
					l2_result->miss_hit = HIT_LOAD;
				} 
				
				return OK;
			}
		}
	}
	//HAY UN MISS EN L1

   	if(l1_result->miss_hit == MISS_LOAD || l1_result->miss_hit == MISS_STORE){

		if(loadstore){//Con miss store, tengo que ir a buscar a L2 y ver si ahi esta, o sea, puede darme hit o miss
			l1_result->miss_hit = MISS_STORE;
			
			//Busco en L2

			//Si tengo hit: marco como dirty y hit store
			for(size_t k = 0; k < l1_l2_info->l2_assoc; k++){
				if(l2_cache_blocks[k].valid){
					if(l2_cache_blocks[k].tag == l1_l2_info->l2_tag){
						l2_cache_blocks[k].dirty = 1;
						l2_result->miss_hit = HIT_STORE;

						for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){
							if(l2_cache_blocks[j].rp_value > l2_cache_blocks[k].rp_value) l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
						}
						l2_cache_blocks[k].rp_value = l1_l2_info->l2_assoc-1;
					}
				}
			}
			
			//Si tengo miss: voy a traerlo de memoria, marco como miss store, marco dirty, y reemplazo segun LRU en L2.
			if(l2_result->miss_hit == MISS_LOAD || l2_result->miss_hit == MISS_STORE){
				
				l2_result->miss_hit = MISS_STORE;
				size_t lru_way = 0;
				for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){ //Busco el LRU
					if(l2_cache_blocks[j].rp_value < l2_cache_blocks[lru_way].rp_value) lru_way = j;
				}

				//FALTA GUARDAR EL EVICTED
				if(l2_cache_blocks[lru_way].valid) {
					l2_result->evicted_address = l2_cache_blocks[lru_way].tag;
					l2_result->dirty_eviction = l2_cache_blocks[lru_way].dirty;
				}
				l2_cache_blocks[lru_way].tag = l1_l2_info->l2_tag;
      			l2_cache_blocks[lru_way].valid = 1;
      			l2_cache_blocks[lru_way].dirty = 1;

				for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){
					if(l2_cache_blocks[j].rp_value > l2_cache_blocks[lru_way].rp_value) l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
				}
				l2_cache_blocks[lru_way].rp_value = l1_l2_info->l2_assoc-1;
				
			}



		}
		else{//Voy a buscar a L2 y ver si ahi esta, puede darme hit o miss
			l1_result->miss_hit = MISS_LOAD;

			//Busco en L2

			//Si tengo hit: marco como hit load
			for(size_t k = 0; k < l1_l2_info->l2_assoc; k++){
				if(l2_cache_blocks[k].valid){
					if(l2_cache_blocks[k].tag == l1_l2_info->l2_tag){
						l2_result->miss_hit = HIT_LOAD;

						for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){
							if(l2_cache_blocks[j].rp_value > l2_cache_blocks[k].rp_value) l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
						}
						l2_cache_blocks[k].rp_value = l1_l2_info->l2_assoc-1;
					}
				}
			}

			//Si tengo miss: traigo de memoria, marco como miss load, y reemplazo segun LRU en L2
			if(l2_result->miss_hit == MISS_LOAD || l2_result->miss_hit == MISS_STORE){
				l2_result->miss_hit = MISS_LOAD;
				size_t lru_way = 0;
				for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){ //Busco el LRU
					if(l2_cache_blocks[j].rp_value < l2_cache_blocks[lru_way].rp_value) lru_way = j;
				}

				if(l2_cache_blocks[lru_way].valid) {
					l2_result->evicted_address = l2_cache_blocks[lru_way].tag;
					l2_result->dirty_eviction = l2_cache_blocks[lru_way].dirty;
				}

				l2_cache_blocks[lru_way].tag = l1_l2_info->l2_tag;
      			l2_cache_blocks[lru_way].valid = 1;

				for(size_t j = 0; j < l1_l2_info->l2_assoc; j++){
					if(l2_cache_blocks[j].rp_value > l2_cache_blocks[lru_way].rp_value) l2_cache_blocks[j].rp_value = l2_cache_blocks[j].rp_value-1;
				}
				l2_cache_blocks[lru_way].rp_value = l1_l2_info->l2_assoc-1;
			}

		}
		

		//Busco LRU L1 para reemplazarlo
		size_t lru_way = 0;
		for(size_t j = 0; j < l1_l2_info->l1_assoc; j++){ //Busco el LRU
			if(l1_cache_blocks[j].rp_value < l1_cache_blocks[lru_way].rp_value) lru_way = j;
		}

		if(l1_cache_blocks[lru_way].valid) l1_result->evicted_address = l1_cache_blocks[lru_way].tag;

		//Reemplazo valores L1
		l1_cache_blocks[lru_way].tag = l1_l2_info->l1_tag;
		l1_cache_blocks[lru_way].valid = 1;

		//Actualizar LRU L1
		for(size_t j = 0; j < l1_l2_info->l1_assoc; j++){
			if(l1_cache_blocks[j].rp_value > l1_cache_blocks[lru_way].rp_value) l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value-1;
		}
		l1_cache_blocks[lru_way].rp_value =l1_l2_info->l1_assoc-1;

		return OK;
   }



   return ERROR;
}
