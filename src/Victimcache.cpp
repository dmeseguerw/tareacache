/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
								bool loadstore,
								entry* l1_cache_blocks,
       	                  	 	entry* vc_cache_blocks,
        	                 	operation_result* l1_result,
								operation_result* vc_result,
                	         	bool debug)
{
	if(l1_vc_info->l1_idx < 0 || l1_vc_info->l1_tag < 0 || l1_vc_info->l1_assoc < 1 || l1_vc_info->vc_assoc < 1){
      return PARAM;
   }

   if(loadstore) {//Comenzamos en miss
               l1_result->miss_hit = MISS_STORE;
               }
   else{
      l1_result->miss_hit = MISS_LOAD;
   }


   // Verificar si hay un hit. SI HAY HIT NO HACE NADA AL VICTIM 	
   for(size_t i = 0; i <l1_vc_info->l1_assoc; i++){//Si valid es 0, no hay nada en el bloque de cache todavia
      if(l1_cache_blocks[i].valid){
         if(l1_cache_blocks[i].tag == l1_vc_info->l1_tag){//Esto es un hit
            if(loadstore) {
               l1_cache_blocks[i].dirty = true;
               l1_result->miss_hit = HIT_STORE;
               }
            else{l1_result->miss_hit = HIT_LOAD;}

            // Update LRUs
            for(size_t j = 0; j < l1_vc_info->l1_assoc; j++){
               if(l1_cache_blocks[j].rp_value > l1_cache_blocks[i].rp_value) l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value-1;
            }
            l1_cache_blocks[i].rp_value = l1_vc_info->l1_assoc-1;
            return OK;
         }
      }

   }


   // Ver si hay un miss
   if(l1_result->miss_hit == MISS_LOAD || l1_result->miss_hit == MISS_STORE){

      //Como hay un miss, busco el LRU para reemplazarlo
      size_t lru_way = 0;
      for(size_t j = 0; j < l1_vc_info->l1_assoc; j++){ //Busco el LRU
         if(l1_cache_blocks[j].rp_value < l1_cache_blocks[lru_way].rp_value) lru_way = j;
      }


      
      //Guardo el evicted
      if(l1_cache_blocks[lru_way].valid){
         l1_result->dirty_eviction = l1_cache_blocks[lru_way].dirty;
         l1_result->evicted_address = l1_cache_blocks[lru_way].tag;
      }
      
      l1_cache_blocks[lru_way].valid = 1;

      // LOGICA VICTIM CACHE
      //Si tengo hit, no reacomodo por ser FIFO
      for(size_t i = 0; i <l1_vc_info->vc_assoc; i++){
         if(vc_cache_blocks[i].valid){
            if(vc_cache_blocks[i].tag == l1_vc_info->l1_tag){
               
               //Se hace un switch entre L1 y VC
               l1_cache_blocks[lru_way].tag = vc_cache_blocks[i].tag;
               l1_cache_blocks[lru_way].dirty = vc_cache_blocks[i].dirty;
               l1_cache_blocks[lru_way].valid = vc_cache_blocks[i].valid;

               vc_cache_blocks[0].tag = l1_result->evicted_address;
               vc_cache_blocks[0].dirty = l1_result->dirty_eviction;
               vc_cache_blocks[0].valid = 1;
               vc_result->miss_hit = (loadstore ? HIT_STORE:HIT_LOAD);
            }
         }
      }

      // Si tengo miss, saco el ultimo y meto el evicted de primero
      if(vc_result->miss_hit == MISS_LOAD || vc_result->miss_hit == MISS_STORE){
         int first = 0;
         if(vc_cache_blocks[l1_vc_info->vc_assoc-1].valid){//Solo si esta lleno
            vc_result->evicted_address = vc_cache_blocks[l1_vc_info->vc_assoc-1].tag;
            vc_result->dirty_eviction = vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty;
            }

         for(size_t j = l1_vc_info->vc_assoc -1; j > 0; j--){
            vc_cache_blocks[j].tag = vc_cache_blocks[j-1].tag;
            vc_cache_blocks[j].dirty = vc_cache_blocks[j-1].dirty;
            vc_cache_blocks[j].valid = vc_cache_blocks[j-1].valid;
         }

         vc_cache_blocks[0].tag = l1_result->evicted_address;
         vc_cache_blocks[0].dirty = l1_result->dirty_eviction;
         vc_cache_blocks[0].valid = 1;

         vc_result->miss_hit = (loadstore ? MISS_STORE:MISS_LOAD);
         l1_cache_blocks[lru_way].tag = l1_vc_info->l1_tag;

      }
      

		// TERMINA VICTIM CACHE


      //Reemplazo valores

      if(loadstore){
         l1_cache_blocks[lru_way].dirty = true;
         l1_result->miss_hit = MISS_STORE;
      }
      else{l1_result->miss_hit = MISS_LOAD;}

      //Reacomodo el LRU
      for(size_t j = 0; j < l1_vc_info->l1_assoc; j++){
         if(l1_cache_blocks[j].rp_value > l1_cache_blocks[lru_way].rp_value) l1_cache_blocks[j].rp_value = l1_cache_blocks[j].rp_value-1;
      }
      l1_cache_blocks[lru_way].rp_value = l1_vc_info->l1_assoc-1;
      
      return OK;

   }
   return ERROR;
}
