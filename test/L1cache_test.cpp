/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i; 
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,hecking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity 
 * 3. Fill a cache entry 
 * 4. Insert a new block A 
 * 5. Force a hit on A 
 * 6. Check rp_value of block A 
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  int status;
  int i;
  int idx_A;
  int tag_A;
  int associativity;
  int rand_policy = rand()%3;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx_A = rand()%1024;
  tag_A = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx_A: %d\n tag_A: %d\n associativity: %d\n RP: %d\n",
          idx_A,
          tag_A,
          associativity,
          rand_policy);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  int M;
  if (associativity <= 2){ M = 1;}
  if (associativity > 2){ M = 2;}
   // Valores para inicializar los bloques en rrip
  int distante = pow(2, M) - 1;
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      if(rand_policy == 0) cache_line[i].rp_value = i;
      if(rand_policy == LRU) cache_line[i].rp_value = 0;
      if(rand_policy == NRU) cache_line[i].rp_value = distante;

      
      while (cache_line[i].tag == tag_A) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    if(rand_policy == LRU) status = lru_replacement_policy(idx_A, 
                                      tag_A, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));
    
    if(rand_policy == NRU) status = nru_replacement_policy(idx_A, 
                                      tag_A, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));

    if(rand_policy == RRIP) status = srrip_replacement_policy(idx_A, 
                                      tag_A, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));

    EXPECT_EQ(status, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag_A now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  int expected_rp;
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    if(rand_policy == LRU) status = lru_replacement_policy(idx_A, 
                                     tag_A, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);

    if(rand_policy == NRU) status = nru_replacement_policy(idx_A, 
                                     tag_A, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    
    if(rand_policy == RRIP) status = srrip_replacement_policy(idx_A, 
                                     tag_A, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);

    EXPECT_EQ(status, 0);
    if(rand_policy==0) expected_rp = associativity-1;
    if(rand_policy==1) expected_rp = 0;
    if(rand_policy==2) expected_rp = 0;
    EXPECT_EQ((int)cache_line[0].rp_value, int(expected_rp)); //Check rp value of block A
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }



  DEBUG(debug_on,hecking fill and eviction operation);
  
  // Fill set
  int idx, tag;
  int expected_evicted = 0;
  for(i = 0; i<associativity; i++){
    idx = rand()%1024;
    tag = rand()%4096;
    loadstore = 0;
    std::cout << "NEW: \n";
    for(int j = 0; j < associativity; j++){
      std::cout << cache_line[j].tag << " | " << (int)cache_line[j].rp_value << "\n";
    }
    if(rand_policy == LRU) status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);

    if(rand_policy == NRU) status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    
    if(rand_policy == RRIP){

      status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
      // if(i==0) expected_evicted = result.evicted_address;
    }

    EXPECT_EQ(status, 0);
  }
  EXPECT_EQ(result.evicted_address, tag_A);
  expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
  EXPECT_EQ(result.miss_hit, expected_miss_hit);
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};
  int policy;

  policy = rand()%3;

  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  int M = 0;
  if(associativity <= 2){
    M = 1;
  }
  else{
    M = 2;
  }
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[2][associativity];

  for(int i = 0; i < 2; i++){
    for (int j =  0; j < associativity; j++) {
      cache_line[i][j].valid = true;
      cache_line[i][j].tag = rand()%4096;
      cache_line[i][j].dirty = 0;
      while (cache_line[i][j].tag == tag) {
        cache_line[i][j].tag = rand()%4096;
      }
      if(policy == LRU){
        cache_line[i][j].rp_value = j;
      }
      else if((policy == NRU) || (policy == RRIP && (M == 1))){
        cache_line[i][j].rp_value = 1;
      }
      else if((policy == RRIP) && (M == 2)){
        cache_line[i][j].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
      if(debug){
        printf("Index: %d, tag: %d, rp_value: %d\n", i, cache_line[i][j].tag, cache_line[i][j].rp_value);
        printf("\n");
      }
    }
  }

  int entered[associativity];
  entered[0] = tag;


  for(int i = 0; i < 2; i++){
    for (int j = 0 ; j < associativity; j++){
      entered[j] = tag + j;
      if(policy == LRU){
        status = lru_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));                                
      }
      else if(policy == NRU){
        status = nru_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));
      }
      else if(policy == RRIP){
        status = srrip_replacement_policy(idx,
                                          entered[j],
                                          associativity,
                                          loadstore,
                                          cache_line[i],
                                          &result,
                                          bool(debug_on));
      }
    }
  }
  if(debug){       
    for(int j = 0; j < associativity; j++){
      printf("entered: %d\n", entered[j]);
    }
  }  
  
  int tag_for_hit = rand()%associativity;
  int block_A = entered[tag_for_hit];
  if(debug){
    printf("forced write hit of: %d in block A\n", block_A);
  }  
  loadstore = 1;

  if(policy == LRU){
    status = lru_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == NRU){
    status = nru_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == RRIP){
    status = srrip_replacement_policy(idx,
                                      entered[tag_for_hit],
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }

  loadstore = 0;
  tag_for_hit = rand()%associativity;
  int block_B = entered[tag_for_hit];
  if(debug){
    printf("forced read hit of: %d in block B\n", block_B);
  }  
  if(policy == LRU){
    status = lru_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == NRU){
    status = nru_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == RRIP){
    status = srrip_replacement_policy(idx,
                                      block_B,
                                      associativity,
                                      loadstore,
                                      cache_line[1],
                                      &result,
                                      bool(debug_on));
  }

  loadstore = 0;
  if(debug){
    printf("forced read hit of: %d in block A\n", block_A);
  }  
  if(policy == LRU){
    status = lru_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));                                
  }
  else if(policy == NRU){
    status = nru_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  else if(policy == RRIP){
    status = srrip_replacement_policy(idx,
                                      block_A,
                                      associativity,
                                      loadstore,
                                      cache_line[0],
                                      &result,
                                      bool(debug_on));
  }
  if(debug){ 
    printf("hasta aqui partes 4-6\n");
    for(int i = 0; i < 2; i++){
      for(int j = 0; j <associativity; j++){
        printf("Index: %d, tag: %d, rp_value: %d. Dirty: %d\n", i, cache_line[i][j].tag, cache_line[i][j].rp_value, cache_line[i][j].dirty);
        printf("\n");
      }
      
    }
  }

  tag = rand() % 250000 + 350000;
  int compare = 0;
  entered[0] = tag;
  int N = 0;

  if((policy == RRIP) && (M == 2)){
    while(result.evicted_address != block_B){
      N++;
      if(N == associativity){
        N = 0;
      }
      tag = rand() % 5200 + 5000;
      status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line[1],
                                          &result,
                                          bool(debug_on));
    }                                             
    if((result.evicted_address == block_B) && (result.dirty_eviction == false)){
            EXPECT_EQ(result.evicted_address, block_B);
            compare = N;
            EXPECT_EQ(result.dirty_eviction, false);
    }
    N = 0;
    while(result.evicted_address != block_A){
      N++;
      if(N == associativity){
        N = 0;
      }
      tag = rand() % 5200 + 5000;
      status = srrip_replacement_policy(idx,
                                          tag,
                                          associativity,
                                          loadstore,
                                          cache_line[0],
                                          &result,
                                          bool(debug_on));
    }                                             
    if((result.evicted_address == block_A) && (result.dirty_eviction == true)){
      EXPECT_EQ(result.evicted_address, block_A);
      compare = N;
      EXPECT_EQ(result.dirty_eviction, true);
    }    
  }

  else{
    for(int i = 0; i < 2; i++){
      for (int j = 0 ; j < associativity; j++){
        entered[j] = tag + j + i*2000;
        if(policy == LRU){
          status = lru_replacement_policy(idx,
                                            entered[j],
                                            associativity,
                                            loadstore,
                                            cache_line[i],
                                            &result,
                                            bool(debug_on));                                
        }
        else if((policy == NRU) || (policy == RRIP && (M == 1))){
          status = nru_replacement_policy(idx,
                                            entered[j],
                                            associativity,
                                            loadstore,
                                            cache_line[i],
                                            &result,
                                            bool(debug_on));
        }
        if(i == 0){
          if(result.evicted_address == block_A){
            EXPECT_EQ(result.evicted_address, block_A);
            compare = j;
            
            EXPECT_EQ(result.dirty_eviction, true);
          }
        } 
        if(i == 1){ 
          if(result.evicted_address == block_B){
            EXPECT_EQ(result.evicted_address, block_B);
            compare = j;
            
            EXPECT_EQ(result.dirty_eviction, false);
          }
        }  
      }
    }
  }  
}



/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  int status;
  int i;
  int associativity;
  int rand_policy = rand()%3;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  associativity = 0;
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking index error);
  loadstore = 0;
  // CAMBIAR RAND POLICY
  if(rand_policy == LRU) status = lru_replacement_policy(-3, 
                                    1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
  
  if(rand_policy == NRU) status = nru_replacement_policy(-3, 
                                    1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  if(rand_policy == RRIP) status = srrip_replacement_policy(-3, 
                                    1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  EXPECT_EQ(status, PARAM);

  DEBUG(debug_on,Checking tag error);
  loadstore = 0;
  if(rand_policy == LRU) status = lru_replacement_policy(4, 
                                    -1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
  
  if(rand_policy == NRU) status = nru_replacement_policy(4, 
                                    -1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  if(rand_policy == RRIP) status = srrip_replacement_policy(4, 
                                    -1, 
                                    2,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  EXPECT_EQ(status, PARAM);

  DEBUG(debug_on,Checking assoc error);
  loadstore = 0;
  if(rand_policy == LRU) status = lru_replacement_policy(4, 
                                    1, 
                                    0,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
  
  if(rand_policy == NRU) status = nru_replacement_policy(4, 
                                    1, 
                                    0,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  if(rand_policy == RRIP) status = srrip_replacement_policy(4, 
                                    1, 
                                    0,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));

  EXPECT_EQ(status, PARAM);
}
